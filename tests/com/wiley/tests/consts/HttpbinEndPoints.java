package com.wiley.tests.consts;

public final class HttpbinEndPoints {
	
	public static final String basePath = "https://httpbin.org";
	public static final String delay = "/delay/%s"; 
	public static final String pngImage = "/image/png";
	
	private HttpbinEndPoints() {}
}
