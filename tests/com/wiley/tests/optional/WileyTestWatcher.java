package com.wiley.tests.optional;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class WileyTestWatcher extends TestWatcher {
	
	@Override
	protected  void	starting(Description description)
	{
		System.out.println("TEST STARTED: " + description);
	}
	
	@Override
	protected  void	succeeded(Description description)
	{
		System.out.println("TEST SUCCEEDED: " + description);
	}
	
	@Override
	protected  void	failed(Throwable e, Description description)
	{
		System.out.println("TEST FAILED: " + description);
		e.printStackTrace();
	}
	
	@Override
	protected  void	finished(Description description)
	{
		System.out.println("TEST FINISHED: " + description + "\n");
	}
	
}
