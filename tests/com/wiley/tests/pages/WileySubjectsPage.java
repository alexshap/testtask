package com.wiley.tests.pages;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WileySubjectsPage {
	
	private String currentSubject;
	
	@FindBy(xpath="//*[@class='wiley-slogan']/h1[3]/span")
	private WebElement header;
	
	@FindBy(xpath="//*[@class='side-panel']/ul/li")
	private List<WebElement> sidePanelSubjects;
	
	WileySubjectsPage(WebDriver driver, String currentSubject)
	{
		PageFactory.initElements(driver, this);
		this.currentSubject = currentSubject;
	}
	
	public WebElement getHeader()
	{	
		return header;
	}
	
	public Set<String> getSidePanelSubjectsList()
	{
		Set<String> itemNamesArr = new HashSet<String>();
		for(WebElement el : sidePanelSubjects)
			itemNamesArr.add(el.getAttribute("textContent"));
		
		return itemNamesArr;
	}
	
}
