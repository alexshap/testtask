package com.wiley.tests.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WileySearchPage {

	@FindBy(xpath="//*[@class='product-title']/a")
	private List<WebElement> titles; 
	
	@FindBy(xpath="//*[@class='tab-content']//div[contains(@id, 'E-Book')]")
	private List<WebElement> EBooks;
	
	@FindBy(xpath="//*[@class='tab-content']//div[contains(@id, 'Print')]")
	private List<WebElement> Prints;
	
	@FindBy(xpath="//*[@class='tab-content']//div[contains(@id, 'O-Book')]")
	private List<WebElement> OBooks;
	
	public WileySearchPage(WebDriver driver)
	{
		(new WebDriverWait(driver, 10))
			.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='product-title']/a")));
		
		PageFactory.initElements(driver, this);
	}
	
	public List<String> getTitlesOfSearchPage()
	{
		List<String> namesOfTitles = new ArrayList<String>();
		for(WebElement el : this.titles)
			namesOfTitles.add(el.getAttribute("textContent"));
		
		return namesOfTitles;
	}
	
	private HashMap<String, Boolean> getHashMapByPattern(List<WebElement> searchingList, String btn_title)
	{
		HashMap<String, Boolean> hashmap = new HashMap<String, Boolean>();
		
		for(WebElement el : searchingList)
		{
			String title = el.findElement(By.xpath("//*[@class='tab-content']//div[contains(@id, '"+el.getAttribute("id")+"')]//ancestor::div[@class='product-content']/h3/a"))
					.getAttribute("textContent");
			
			hashmap.put(title, 
					el.findElement(By.
							xpath("//*[@id='"+el.getAttribute("id")+"']//*[@class='product-button']")).getAttribute("textContent").contains(btn_title));
		}
		
		return hashmap;
	}
	
	public HashMap<String, Boolean> getEBooksNumOfBtns(String btn_title)
	{
		return getHashMapByPattern(EBooks, btn_title);
	}
	
	public HashMap<String, Boolean> getPrintNumOfBtns(String btn_title)
	{
		return getHashMapByPattern(Prints, btn_title);
	}
	
	public HashMap<String, Boolean> getOBooksNumOfBtns(String btn_title)
	{	
		return getHashMapByPattern(OBooks, btn_title);
	}
	
}
