package com.wiley.tests.pages;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WileyHeader {
	
	private WebDriver driver;
	
	@FindBy(id="main-header-navbar")
	private WebElement navbar;
	
	@FindBy(id="js-site-search-input")
	private WebElement searchInputField;
	
	@FindBy(xpath="//*[@id=\"main-header-container\"]/div/div[2]/div/form/div/span/button")
	private WebElement searchBtn;
	
	public WileyHeader(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	private List<WebElement> getDropDownSubMenuByTitle(String title)
	{
		return navbar.findElements(By.xpath("//span[contains(text(), '" + title +"')]/following-sibling::ul/li/a"));
	}
	
	public Set<String> getDropDownSubMenuItemsByTitle(String title)
	{
		List<WebElement> subMenuEls = getDropDownSubMenuByTitle(title);
		
		Set<String> itemNamesArr = new HashSet<String>();
		for(WebElement el : subMenuEls)
			itemNamesArr.add(el.getAttribute("textContent").trim());
		
		return itemNamesArr;
	}
	
	public WileySearchPage searchEnteredInfo(String inputText)
	{
		searchInputField.sendKeys(inputText);
		searchBtn.submit();
		
		return new WileySearchPage(driver);
	}
	
	public WileySubjectsPage getSubjectPage(String menuitem)
	{
		for(WebElement el : getDropDownSubMenuByTitle("SUBJECTS"))
			if(el.getAttribute("textContent").trim().contentEquals(menuitem))
			{
				driver.get(el.getAttribute("href"));
				break;
			}
		
		return new WileySubjectsPage(driver, menuitem);
	}
	
	public List<String> getSuggestionsByString(String inputText)
	{
		(new WebDriverWait(driver, 10))
			.until(ExpectedConditions.visibilityOfElementLocated(By.id("js-site-search-input")));
		searchInputField.sendKeys(inputText);
		(new WebDriverWait(driver, 10))
			.until(ExpectedConditions.visibilityOfElementLocated(By.className("main-navigation-search-autocomplete")));
		
		List<String> returnedSuggestionsList = new ArrayList<String>();
		List<WebElement> searchHighlights = driver.findElements(By.className("search-highlight"));
		for(WebElement el : searchHighlights)
			returnedSuggestionsList.add(el.getAttribute("textContent"));
		
		return returnedSuggestionsList;
	}
	
	
	
}
