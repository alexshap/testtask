package com.wiley.tests;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.*;
import org.junit.rules.TestWatcher;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.wiley.tests.optional.WileyTestWatcher;
import com.wiley.tests.pages.WileyHeader;
import com.wiley.tests.pages.WileySearchPage;
import com.wiley.tests.pages.WileySubjectsPage;

public class FirstPart_WileyPagesUITests {
	
	private static FileInputStream fis;
	private static WebDriver driver;
	private static Properties configs;
	private WileyHeader header;
	
	static
	{
		try {
			FileInputStream fis = new FileInputStream("conf.properties");
			configs = new Properties();
			configs.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		} finally
		{
			if(fis != null)
			{
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@Rule
	public TestWatcher watchman = new WileyTestWatcher();
	
	@BeforeClass
	public static void setUp() throws IOException
	{
		System.out.println("Wiley page tests (PART 1) started...");
		
		if(configs.getProperty("useDockerRemoteWebDriver").toLowerCase().equals("true"))
		{
			ChromeOptions chr_opts = new ChromeOptions();
			driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), chr_opts);
		} else
		{
			System.setProperty("webdriver.chrome.driver", configs.getProperty("chromedriver"));
			driver = new ChromeDriver();
		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	@Before
	public void setUpStartPage()
	{
		driver.get("https://www.wiley.com/en-us");
		header = new WileyHeader(driver);
	}
	
	@Test
	public void WhoWeServeItems_isEqualsSet()
	{
		Set<String> expectedSet = new HashSet<String>(Arrays.asList(
				"Students", "Instructors",
				"Book Authors", "Professionals", "Researchers",
				"Institutions", "Librarians", "Corporations", 
				"Societies", "Journal Editors", "Government"));
		
		Set<String> actualSet = header.getDropDownSubMenuItemsByTitle("WHO WE SERVE");
		
		Assert.assertEquals("'WHO WE SERVE' sub-menu items are not corresponded to expectedSet;", expectedSet, actualSet);
	}
	
	@Test
	public void SearchSuggestions_isRelatedContent()
	{
		String enteredStr = "Java";
		List<String> actualContent = header.getSuggestionsByString(enteredStr);
		
		boolean isRelated = !actualContent.isEmpty();
		for(String content : actualContent)
			if(!content.toLowerCase().equals(enteredStr.toLowerCase()))
			{
				isRelated = false;
				break;
			}
				
		Assert.assertTrue("Content is not related to entered string ("+enteredStr+")", isRelated);
	}
	
	@Test
	public void SearchResult_isRelatedContentAndNumOfTitles()
	{
		String enteredStr = "Java";
		int expectedNumOfTitles = 10;
		WileySearchPage searchpg = header.searchEnteredInfo(enteredStr);
		
		List<String> titles = searchpg.getTitlesOfSearchPage();
		boolean isRelated = !titles.isEmpty();
		for(String title : titles)
			if(!title.toLowerCase().matches("(.*)"+enteredStr.toLowerCase()+"(.*)"))
			{
				isRelated = false;
				break;
			}
		
		Assert.assertTrue("Searched content is not related to entered string ("+enteredStr+")", isRelated);
		Assert.assertTrue("Number of titles is not corresponded to expected one (exp: "+expectedNumOfTitles+", but was: "+titles.size()+");", 
				titles.size() == expectedNumOfTitles);
	}
	
	private List<String> checkProductsButtonByHashMap(HashMap<String, Boolean> hashmap)
	{
		List<String> emptyCartTitles = new ArrayList<String>(); 
		for(String key : hashmap.keySet())
			if(!hashmap.get(key))
				emptyCartTitles.add(key);
		
		return emptyCartTitles;
	}
	
	@Test
	public void ProductsButton_failIfEmpty()
	{
		WileySearchPage searchpg = header.searchEnteredInfo("Java");
		
		List<String> ebookTitles = checkProductsButtonByHashMap(searchpg.getEBooksNumOfBtns("Add to cart"));
		List<String> printBtns = checkProductsButtonByHashMap(searchpg.getPrintNumOfBtns("Add to cart"));
		List<String> obooks = checkProductsButtonByHashMap(searchpg.getOBooksNumOfBtns("View on Wiley Online Library"));
		
		List<String> expectedList = new ArrayList<String>();
		
		Assert.assertEquals("E-Books have title without product button ("+ebookTitles.toString()+")", expectedList, ebookTitles);
		Assert.assertEquals("Prints have title without product button ("+printBtns.toString()+")", expectedList, printBtns);
		Assert.assertEquals("O-Books have title without product button ("+obooks.toString()+")", expectedList, obooks);
	}
	
	@Test
	public void EducationSbjct_isDisplayedHdrAndItms()
	{
		driver.get("https://www.wiley.com/en-us");
		WileyHeader header = new WileyHeader(driver);
		String currSubj = "Education";
		WileySubjectsPage subjpage = header.getSubjectPage(currSubj);
		Set<String> expectedSet = new HashSet<String>(Arrays.asList("Information & Library Science",
				"Education & Public Policy", "K-12 General", "Higher Education General", "Vocational Technology", 
				"Conflict Resolution & Mediation (School settings)",
				"Curriculum Tools- General", "Special Educational Needs",
				"Theory of Education", "Education Special Topics", "Educational Research & Statistics",
				"Literacy & Reading", "Classroom Management"));
		
		Set<String> actualSet = subjpage.getSidePanelSubjectsList();
		
		Assert.assertTrue("Header is not displayed;", subjpage.getHeader().isDisplayed());
		Assert.assertTrue("Header is not corresponded to selected link", subjpage.getHeader().getAttribute("textContent").equals(currSubj));
		Assert.assertEquals("Subject '"+currSubj+"' items are not corresponded to expectedSet;", expectedSet, actualSet);
	}
	
	@AfterClass
	public static void tearDown()
	{
		driver.quit();
		System.out.println("Wiley pages tests finished...");
	}

}
