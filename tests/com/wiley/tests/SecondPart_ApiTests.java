package com.wiley.tests;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.*;
import org.junit.rules.TestWatcher;

import com.wiley.tests.consts.HttpbinEndPoints;
import com.wiley.tests.optional.WileyTestWatcher;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertArrayEquals;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class SecondPart_ApiTests {

	private static final String pathToExpectedImage = "optionalsources/expectedPig.png";
	private static final String pathToPostDelayEndPointResponseSchema = "responsePostDelaySchema.json";
	private static final String pathToWileyGetRequestResponseSchema = "wileyGetRequestResponseSchema.json";
	private static final String wileyGetRequestAddress = "https://www.wiley.com/en-us/search/autocomplete/comp_00001H9J?term=Java";
	
	@Rule
	public TestWatcher watchman = new WileyTestWatcher();
	
	@BeforeClass
	public static void setUp() throws IOException
	{
		System.out.println("API tests (PART 2) started...");
		RestAssured.baseURI = HttpbinEndPoints.basePath;
	}
	
	@Test
	public void WileyGetRequest_responseMatchesToJSONSchema_TrueReturned()
	{
		get(wileyGetRequestAddress).
		then().
			statusCode(200).
		and().
			contentType("application/json").
		assertThat().
			body(matchesJsonSchemaInClasspath(pathToWileyGetRequestResponseSchema));
	}

	@Test
	public void PostDelay_isCorrectDelay_TrueReturned()
	{
		long expectedDelay = 5;
		long upperBoundDelay = 15;
		
		when().
			post(String.format(HttpbinEndPoints.delay, expectedDelay)).
		then().
			statusCode(200).
		and().
			time(greaterThanOrEqualTo(expectedDelay), TimeUnit.SECONDS).
		and().
			time(lessThan(upperBoundDelay), TimeUnit.SECONDS);
	}
	
	@Test
	public void PostDelay_whenDelayGreaterMax_Less14SecReturned()
	{
		long currentDelay = 60;
		long expectedMaxDelay = 15;
		
		when().
			post(String.format(HttpbinEndPoints.delay, currentDelay)).
		then().
			statusCode(200).
		and().
			time(lessThanOrEqualTo(expectedMaxDelay), TimeUnit.SECONDS);
	}
	
	@Test
	public void PostDelayResponseBody_matchestJSONSchema_TrueReturned()
	{
		post(String.format(HttpbinEndPoints.delay, 1)).
		then().
			statusCode(200).
		and().
			contentType("application/json").
		assertThat().
			body(matchesJsonSchemaInClasspath(pathToPostDelayEndPointResponseSchema));
	}
	
	@Test
	public void GetImage_MatchImgs_endPointImgMatchesToExpected()
	{
		byte[] expectedBlob = {};
		try
		{
			expectedBlob = Files.readAllBytes(new File(pathToExpectedImage).toPath());
		} catch(IOException e)
		{
			e.printStackTrace();
		}
		Response imgEndPointResponse = get(HttpbinEndPoints.pngImage).andReturn();
		
		imgEndPointResponse.then().
			statusCode(200).
		and().
			contentType("image/png");
		
		assertArrayEquals(expectedBlob, imgEndPointResponse.asByteArray());
	}
	
	@AfterClass
	public static void tearDown()
	{
		System.out.println("API (Part 2) tests finished...");
	}
	
}
