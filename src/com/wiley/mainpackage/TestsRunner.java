package com.wiley.mainpackage;

import org.junit.runner.JUnitCore;

public class TestsRunner {

	public static void main(String[] args) {
		for(int i = 0; i < args.length; i++)
			args[i] = "com.wiley.tests." + args[i];
		
		JUnitCore.main(args);
	}

}
